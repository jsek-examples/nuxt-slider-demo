# nuxt-slider-demo

Project based on Nuxt.js 1.0.0

Example usage of component transition with loading indicator.

## Build Setup

``` bash
# install dependencies
$ yarn

# serve with hot reload at localhost:3000
$ yarn dev
```

See [Live Demo](https://nuxt-slider-demo.now.sh)

![](demo.gif)